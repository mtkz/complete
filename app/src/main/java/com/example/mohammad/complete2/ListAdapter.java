package com.example.mohammad.complete2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mohammad on 7/17/2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ListData[] listData;

    public ListAdapter(ListData[] listData) {
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemlayoutview = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item,parent,false);
        ViewHolder viewholder = new ViewHolder(itemlayoutview);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.imageviewitem.setImageResource(listData[position].getImageurl());
        holder.tvtitle.setText(listData[position].getItemtitle());
        holder.tvdesc.setText(listData[position].getItemdesc());

    }

    @Override
    public int getItemCount() {
        return listData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

         private ImageView imageviewitem;
         private TextView tvtitle,tvdesc;


        public ViewHolder(View itemView) {
            super(itemView);

            imageviewitem = (ImageView) itemView.findViewById(R.id.item_image);
            tvtitle = (TextView) itemView.findViewById(R.id.item_title);
            tvdesc = (TextView) itemView.findViewById(R.id.item_desc);
        }
    }
}
