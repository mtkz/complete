package com.example.mohammad.complete2;

/**
 * Created by mohammad on 7/17/2018.
 */

public class ListData {
    private String itemtitle,itemdesc;
    private int imageurl;

    public ListData(String itemtitle, String itemdesc, int imageurl) {

        this.itemtitle = itemtitle;
        this.itemdesc = itemdesc;
        this.imageurl = imageurl;
    }

    public String getItemtitle() {
        return itemtitle;
    }

    public void setItemtitle(String itemtitle) {
        this.itemtitle = itemtitle;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc;
    }

    public int getImageurl() {
        return imageurl;
    }

    public void setImageurl(int imageurl) {
        this.imageurl = imageurl;
    }


}
