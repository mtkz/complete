package com.example.mohammad.complete2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by mohammad on 7/17/2018.
 */

public class PagerAdapater extends FragmentStatePagerAdapter {
    private int pagenum;
    public PagerAdapater(FragmentManager fm,int pagenum) {
        super(fm);
        this.pagenum = pagenum;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Tab1 tab1 = new Tab1();
                return tab1;
            case 1:
                Tab2 tab2 = new Tab2();
                return tab2;
            case 2:
                Tab3 tab3 = new Tab3();
                return tab3;
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return pagenum;
    }
}
