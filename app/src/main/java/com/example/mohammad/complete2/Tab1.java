package com.example.mohammad.complete2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by mohammad on 7/17/2018.
 */

public class Tab1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_1,container,false);

        ListData listData[] ={
                new ListData("test1","testestest",R.drawable.a),
                new ListData("test2","testestest",R.drawable.a),
                new ListData("test3","testestest",R.drawable.a),
                new ListData("test4","testestest",R.drawable.a),
                new ListData("test5","testestest",R.drawable.a)
        };




        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mlayoutmanger = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mlayoutmanger);

        ListAdapter listAdapter = new ListAdapter(listData);
        recyclerView.setAdapter(listAdapter);

        return view;
    }

}
